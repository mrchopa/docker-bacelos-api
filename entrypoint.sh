#!/bin/sh

if [ $ENV_PROFILE == "product" ]
then
  echo "running production enviroment"
  java -Dspring.profiles.active=prd -Duser.timezone=Asia/Seoul -Dlog.path=/var/log/bacelos-api  -jar /usr/local/bin/Bacelos-api.jar 
else
  java -Duser.timezone=Asia/Seoul -Dlog.path=/var/log/bacelos-api  -jar /usr/local/bin/Bacelos-api.jar 
fi
