FROM java:8-alpine
MAINTAINER jaekwon park <jaekwon.park@code-post.com>

# ENV_BOF


# ENV_EOF

ADD $FILENAME	/usr/local/bin/Bacelos-api.jar
ADD kvm_plugin	/usr/local/bin/kvm_plugin
ADD entrypoint.sh /usr/local/bin/entrypoint.sh

RUN apk update && apk upgrade --available && apk add openssh-client && chmod +x /usr/local/bin/kvm_plugin /usr/local/bin/entrypoint.sh

VOLUME /var/log/bacelos-api

EXPOSE 8082

CMD ["/usr/local/bin/entrypoint.sh"]
